import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-test',
  // if set to true then the app-test html is shown (html element is rendered), if set to false then it will not be shown in the dom. however setting true or false is not much use.
  // template: ` <h2 *ngIf="false">Codevolution</h2> `,
  // Lets assign a prperty value instead (below), we can then add or remove element in the dom by toggling between true/false.
  // with the else block - firstly checks if displayName is true, if it is then the element to which the ngIf directive is attached will be rendered in the dom. if it is false however, it checks if there is an else statement, in this case there is! It checks which html element needs to be renederd in this case it is the else block - which is a reference to the ng-template code block below.
  template: `
    <!-- ngIf -->
    <!-- <h2 *ngIf="displayName; else elseBlock">Codevolution</h2> -->
    <!-- reference to the ng-template is elseBlock -->
    <!-- <ng-template #elseBlock>
      <h2>Name is hidden</h2>
    </ng-template> -->
    <!-- alternate syntax for the above: if displayName is true, render then block, else render elseBlock-->
    <!-- <div *ngIf="displayName; then thenBlock; else elseBlock"></div>
    <ng-template #thenBlock>
      <h2>Codevolution</h2>
    </ng-template>
    <ng-template #elseBlock>
      <h2>Hidden</h2>
    </ng-template> -->

    <!-- ngSwitch -->
    <!-- will be 'red' as public color variable is red -->
    <!-- <div [ngSwitch]="color">
      <div *ngSwitchCase="'red'">You picked red color</div>
      <div *ngSwitchCase="'blue'">You picked blue color</div>
      <div *ngSwitchCase="'green'">You picked green color</div>
      <div *ngSwitchDefault>Pick again!</div>
    </div> -->

    <!-- ngFor -->
    <!-- loop through colors array and display index number-->
    <!-- <div *ngFor="let color of colors; index as i"> -->
    <!-- indicates if element is the first element in array or not, can do same with last, odd and even -->
    <div *ngFor="let color of colors; first as f">
      <h2>{{ f }} {{ color }}</h2>
    </div>
    <!-- </div> -->
    <h2>{{ 'Hello ' + parentData }}</h2>
    <button (click)="fireEvent()">Send Event</button>
    <!-- pipes for strings -->
    <h2>{{ name }}</h2>
    <h2>{{ name | lowercase }}</h2>
    <h2>{{ name | uppercase }}</h2>
    <h2>{{ message | titlecase }}</h2>
    <!-- display part of string: slice pipe -->
    <h2>{{ name | slice: 3:5 }}</h2>
    <!-- see json representation of object -->
    <h2>{{ person | json }}</h2>

    <!-- pipes for numbers -->
    <!-- takes one argument in specific format. In the pipe string, first number is minimum number of integer digits, second number is the minimum number of decimal digits, then the last number is the maximum number of decimal digits. -->
    <h2>{{ 5.678 | number: '1.2-3' }}</h2>
    <h2>{{ 5.678 | number: '3.4-5' }}</h2>
    <h2>{{ 5.678 | number: '3.1-2' }}</h2>
    <!-- percentage pipe -->
    <h2>{{ 0.25 | percent }}</h2>
    <!-- currency pipe -->
    <h2>{{ 0.25 | currency }}</h2>
    <h2>{{ 0.25 | currency: 'GBP' }}</h2>
    <h2>{{ date | date: 'short' }}</h2>
    <h2>{{ date | date: 'shortDate' }}</h2>
    <h2>{{ date | date: 'shortTime' }}</h2>
    <!-- there are also long and medium formats for date -->
  `,
  styles: [],
})
export class TestComponent {
  displayName = true;
  public color = 'pink';
  public colors = ['red', 'blue', 'green', 'yellow'];
  public name = 'Codevolution';
  public message = 'Welcome to codevolution';
  public date = new Date();
  public person = {
    firstname: 'John',
    lastname: 'Doe',
  };
  @Input() public parentData: any;
  //sending data from this child component to parent component needs to be done via an event, using output decorator.
  @Output() public childEvent: any = new EventEmitter();
  fireEvent() {
    this.childEvent.emit('Hey Codevolution');
  }
}

// COMPONENT INTERACTION:
// test component is nested inside the app Component, so the app component is the parent component and the test componeen tis the child Component. Can send data from one to anohter with the inout/output decorators.

// parent -> child
// we will send a name from the app component to the test componenet, in the test componenet we will display hello followed by the name. In parent html we have child component selector so you can easily bind the data this way.

// child -> parent
// we will also send data the other way so we will send from the test component to the app component ( a message ) and display it in the app component. In the child compone  you do not have the parent component selector so we can do it in the same way, we have to use 'events'
